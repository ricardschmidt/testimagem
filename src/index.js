require("dotenv").config()
const express = require("express")
const mongoose = require("mongoose")
const morgan = require("morgan")
const cors = require("cors")
const path = require('path');

class App {
	constructor() {
		this.express = express();
		this.database();
		this.middlewares();
		this.routes();

		this.express.listen(process.env.PORT || 3000)
	}

	database() {
		mongoose.connect(process.env.MONGO_URL, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})
	}

	middlewares() {
		this.express.use(cors())
		this.express.use(express.json())
		this.express.use(express.urlencoded({ extended: true }))
		this.express.use(morgan('dev'))
	}

	errorHandler(err, req, res, next) {
		res.status(err.status ? err.status : 500).json({
			error: {
				name: err.name,
				message: err.message,
				userMessage: err.userMessage ? err.userMessage : "Erro inesperado tente novamente mais tarde",
				url: req.url,
				stack: err.stack
			}
		});
	}

	routes() {
		this.express.use(require("./routes"))
		this.express.use(this.errorHandler);
	}
}

module.exports = new App().express
