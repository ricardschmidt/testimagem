
class MalformattedError extends Error {
	constructor() {
		super('Params Malformatted');
		this.status = 400
		this.name = this.constructor.name;
		this.userMessage = 'Parametros mal formatados.'
	}
}

module.exports = MalformattedError
