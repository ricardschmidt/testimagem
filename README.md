# Imagem Challenge

Company Image Challenge Exercises

## Exercises:

- [x] Validate CNPJ format and check digits
- [x] Test if two rectangles intersect
- [x] Compute the area of intersection between two rectangles
- [x] Simple Todo List
- [x] Rest Client - World Clock
- [x] Rest Server - World Clock
- [x] Entity Relationship Diagram - Simple Order Manager

## Quick start

Quick start options:

- Clone the repo: `git clone https://gitlab.com/ricardschmidt/testimagem.git`.
- [Download from Gitlab](https://gitlab.com/ricardschmidt/testimagem/-/archive/main/testimagem-main.zip).
- Run `npm install` or `yarn install`
- Run `npm run dev` or `yarn dev` to start a local development server


## Technologies Used

[Node.js](https://nodejs.org/en/) as compiler.
[Axios 0.27.2](https://axios-http.com/ptbr/docs/intro) for HTTP client.
[Express 4.17.2](https://expressjs.com/pt-br/) for API.
[Mongoose 6.4](https://mongoosejs.com/) for database connection.

## File Structure

Within the download you'll find the following directories and files:

```
Imagem
|-- src
	|-- index.js
	|-- routes.js
	|-- controllers
	|	|-- TaskController.js
	|-- errors
	|   |-- CnpjLengthError.js
	|   |-- InvalidCnpj.js
	|   |-- TaskNotFoundError.js
	|-- models
	|   |-- Task.js
	|-- utils
	|   |-- queryUtils.js
	|   |-- Test.js
```

## Endpoints

**GET**: `/cnpj?cnpj={cnpj}`
e.g: `/cnpj?cnpj={76.306.175/0001-93}`

When status code `200`

```
{
	"status": "OK"
}
```

When status code `400`

```
{
	"error": {
		"name": "InvalidCnpj",
		"message": "Invalid CNPJ.",
		"userMessage": "O CNPJ informado não é válido.",
		"url": "/cnpj?cnpj=76.306.175%2F0001-92"
	}
}
```

**OR**
```
{
	"error": {
		"name": "CnpjLengthError",
		"message": "Error with character number in CNPJ.",
		"userMessage": "O CNPJ não contem o número correto de caracteres.",
		"url": "/cnpj?cnpj=76.306.175%2F0001-9"
	}
}
```

**GET**: `/intersection/count?a={coordinates}&b={coordinates}`
e.g: `intersection/count?a=3,5%3B11,11&b=7,2%3B13,7`

Coordinate Formatting
`10,2;13,8`

When status code `200`

Return the number of intersections points for ex.: `15`

When status code `400`

```
{
	"error": {
		"name": "MalformattedError",
		"message": "Params Malformatted",
		"userMessage": "Parametros mal formatados.",
		"url": "/intersection/count?a=3,5%3B11&b=7,2%3B13,7"
	}
}
```

**GET**: `/intersection?a={coordinates}&b={coordinates}`
e.g: `intersection/count?a=3,5%3B11,11&b=7,2%3B13,7`

Coordinate Formatting
`10,2;13,8`

When status code `200`

Return `true` when it has intersection or `false` if it does not

When status code `400`

```
{
	"error": {
		"name": "MalformattedError",
		"message": "Params Malformatted",
		"userMessage": "Parametros mal formatados.",
		"url": "/intersection/count?a=3,5%3B11&b=7,2%3B13,7"
	}
}
```

**GET**: `/tasks`
Returns all created tasks

When status code `200`

```
[
	{
		"_id": "62b34ad187d5980d14805656",
		"title": "Comprar Arroz",
		"isDone": false,
		"createdAt": "2022-06-22T17:01:05.324Z",
		"updatedAt": "2022-06-22T17:01:05.324Z",
		"__v": 0
	},
	{
		"_id": "62b34adf87d5980d1480565a",
		"title": "Lavar a louça",
		"isDone": false,
		"createdAt": "2022-06-22T17:01:19.501Z",
		"updatedAt": "2022-06-22T17:01:19.501Z",
		"__v": 0
	}
]
```

**POST**: `/tasks`
Creates a new task

Body structure:
```
{
	"title": "Comprar Farinha",
	"isDone": true //default false
}
```

When status code `200`

```
{
	"title": "Comprar Farinha",
	"isDone": false,
	"_id": "62b361a17aa58aa123916846",
	"createdAt": "2022-06-22T18:38:25.031Z",
	"updatedAt": "2022-06-22T18:38:25.031Z",
	"__v": 0
}
```

**PUT**: `/tasks/:id`
Updates a task

Body structure:
```
{
	"title": "Comprar Farinha",
	"isDone": true
}
```

Send only the parameter that you want to update

When status code `200`

```
{
	"confirm": {
		"message": "Task updated successfully",
		"data": {
			"_id": "62b362137aa58aa123916848",
			"title": "Comprar Farinha",
			"isDone": true,
			"createdAt": "2022-06-22T18:40:19.301Z",
			"updatedAt": "2022-06-22T18:40:19.301Z",
			"__v": 0
		}
	}
}
```

When status code `404`

```
{
	"error": {
		"name": "TaskNotFoundError",
		"message": "Task not found.",
		"userMessage": "Task não encontrado.",
		"url": "/tasks/62b34ad187d5980d14805657"
	}
}
```

**DELETE**: `/tasks/:id`
Deletes a task

When status code `200`

```
{
	"confirm": {
		"message": "Task deleted successfully",
		"task": {
			"_id": "62b34ad187d5980d14805656",
			"title": "Comprar Arroz",
			"isDone": false,
			"createdAt": "2022-06-22T17:01:05.324Z",
			"updatedAt": "2022-06-22T17:01:05.324Z",
			"__v": 0
		}
	}
}
```

When status code `404`

```
{
	"error": {
		"name": "TaskNotFoundError",
		"message": "Task not found.",
		"userMessage": "Task não encontrado.",
		"url": "/tasks/62b34ad187d5980d14805657"
	}
}
```

**GET**: `/time`
Makes a request to worldtimeapi and returns the the current time and UTC time

When status code `200`

```
{
	"currentDateTime": "2022-06-22T15:44:40.213686-03:00",
	"utcDateTime": "2022-06-22T18:44:40.213686+00:00"
}
```

## Entity Relationship Diagram - Simple Order Manager

![Diagram](https://gitlab.com/ricardschmidt/testimagem/-/blob/master/img/diagrama.jpeg)
