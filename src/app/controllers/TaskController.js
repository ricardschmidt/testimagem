require("dotenv").config()

const { getMatch, getSort, getSelect } = require("../utils/queryUtils")

const 	TaskNotFoundError = require('../errors/TaskNotFoundError')

const Task = require("../models/Task")

class DriverController {
	async store(req, res, next) {
		try {
			if(req.boby instanceof Array) {
				const data = await Task.insertMany(req.body)
				return res.json(data)
			} else {
				const data = await Task.create(req.body)
				return res.json(data)
			}
		} catch (error) {
			next(error)
		}
	}

	async index(req, res, next) {
		try {
			const {pageSize = 0, page = 0, sort, select } = req.query
			let data = await Task.find(
				{...getMatch(req)}, getSelect(select)
				).sort(getSort(sort))
				.limit(pageSize)
				.skip(pageSize * page);
			return res.json(data)
		} catch (error) {
			next(error)
		}
	}

	async getById(req, res, next) {
		try {
			let data = await Task.findById(req.params.id);
			return res.json(data)
		} catch(error) {
			next(error)
		}
	}

	async count(req, res, next) {
		try {
			const count = await Task.count({...getMatch(req)})
			return res.json(count)
		} catch (error) {
			next(error)
		}
	}

	async update(req, res, next) {
		const { title, isDone } = req.body
		try {
			const data = await Task.findById(req.params.id)
			if(!data) throw new TaskNotFoundError();

			if(title) data.title = title
			if(typeof isDone !== 'undefined') data.isDone = isDone

			data.save()

			return res.json({
				confirm: {
					message: `Task updated successfully`,
					data
				}
			})
		} catch (error) {
			next(error)
		}
	}

	async delete(req, res, next) {
		try {
			let task = await Task.findById(req.params.id)

			if(!task) throw new TaskNotFoundError()
			task.remove()
			return res.json({
				confirm: {
					message: `Task deleted successfully`,
					task
				}
			})
		} catch (error) {
			next(error)
		}
	}

}

module.exports = new DriverController();
