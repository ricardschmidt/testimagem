const routes = require("express").Router();

const Test = require('./app/utils/Test');
const TaskController = require('./app/controllers/TaskController');

/* Endpoint sem autenticação */
routes.get("/cnpj", Test.cnpjValidator);
routes.get("/intersection/count", Test.intersectCount.bind(Test));
routes.get("/intersection", Test.intersect.bind(Test));

routes.get("/tasks", TaskController.index);
routes.post("/tasks", TaskController.store);
routes.put("/tasks/:id", TaskController.update);
routes.delete("/tasks/:id", TaskController.delete);

routes.get("/time", Test.getTime);


routes.use(function(req, res) {
	res.status(404).json({
		error: {
			type: "Route not found",
			message: "404 You're beyond the borders.",
			userMessage: "Ops... Essa rota não existe.",
			url: req.url,
		}
	})
})

module.exports = routes;
