
class TaskNotFoundError extends Error {
	constructor() {
		super('Task not found.');
		this.status = 404
		this.name = this.constructor.name;
		this.userMessage = 'Task não encontrado.'
	}
}

module.exports = TaskNotFoundError
