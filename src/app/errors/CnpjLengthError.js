class CnpjLengthError extends Error {
	constructor(message, userMessage) {
		super(message);
		this.status = 400
		this.name = this.constructor.name;
		this.userMessage = userMessage
	}
}

module.exports = CnpjLengthError
