const 	CnpjLengthError = require('../errors/CnpjLengthError'),
		InvalidCnpj = require('../errors/InvalidCnpj'),
		MalformattedError = require('../errors/MalformattedError')

const axios = require('axios');
const res = require('express/lib/response');

class Test {
	async cnpjValidator(req, res, next){
		try {
			const cnpj = req.query.cnpj.replace(/[^\d]/g, '')
			let digit = [ 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 ]

			if(cnpj.length !== 14)
			throw new CnpjLengthError("Error with character number in CNPJ.", "O CNPJ não contem o número correto de caracteres.")

			if(/0{14}/.test(cnpj))
			throw new InvalidCnpj("Invalid CNPJ.", "O CNPJ informado não é válido.")

			let dv1 = 0
			for (let i = 0; i < 12; i++) {
				dv1 += cnpj[i] * digit[i +1]
			}
			if(cnpj[12] != (((dv1 %= 11) < 2) ? 0 : 11 - dv1))
			throw new InvalidCnpj("Invalid CNPJ.", "O CNPJ informado não é válido.")

			let dv2 = 0
			for (let i = 0; i <= 12; i++) {
				dv2 += cnpj[i] * digit[i]
			}
			if(cnpj[13] != (((dv2 %= 11) < 2) ? 0 : 11 - dv2))
			throw new InvalidCnpj("Invalid CNPJ.", "O CNPJ informado não é válido.")

			res.json({status: "OK"})
		} catch (error) {
			next(error)
		}
	}

	async intersectCount(req, res, next) {
		try{
			res.json(this.intersectCheck(req))
		} catch (error) {
			next(error)
		}
	}

	async intersect(req, res, next) {
		try{
			this.intersectCheck(req) === 0 ? res.json(false) : res.json(true)
		} catch (error) {
			next(error)
		}
	}

	async getTime(req, res, next) {
		try {
			axios.get("http://worldtimeapi.org/api/ip")
			.then( response => {
				res.json({
					currentDateTime: response.data.datetime,
					utcDateTime: response.data.utc_datetime
				})
			}).catch( error => {
				res.json(error)
			})
		} catch(error) {
			next(error)
		}
	}

	intersectCheck(req) {
		let { a, b } = req.query
		a = a.split(";")
		b = b.split(";")

		if(a.length !== 2 || b.length !== 2) throw new MalformattedError()
		if(a[0].split(",").length !== 2 || a[1].split(",").length !== 2 || b[0].split(",").length !== 2 || b[1].split(",").length !== 2) throw new MalformattedError()

		let aDots = []
		let bDots = []
		for(let i = parseInt(a[0].split(",")[0]); i <= parseInt(a[1].split(",")[0]); i++) {
			for(let j = parseInt(a[0].split(",")[1]); j <= parseInt(a[1].split(",")[1]); j++) {
				aDots.push(`${i}, ${j}`)
			}
		}
		for(let i = parseInt(b[0].split(",")[0]); i <= parseInt(b[1].split(",")[0]); i++) {
			for(let j = parseInt(b[0].split(",")[1]); j <= parseInt(b[1].split(",")[1]); j++) {
				bDots.push(`${i}, ${j}`)
			}
		}
		let count = 0
		if(aDots.length > bDots.length) {
			for(let i = 0; i < bDots.length; i++) {
				if(aDots.includes(bDots[i])) count++
			};
		} else {
			for(let i = 0; i < aDots.length; i++) {
				if(bDots.includes(aDots[i])) count++
			};
		}

		return count
	}
}

module.exports = new Test();
